# Module controls-server

A combined Magix event loop server with web server for visualization.

## Usage

## Artifact:

The Maven coordinates of this project are `space.kscience:controls-server:0.2.0`.

**Gradle Kotlin DSL:**
```kotlin
repositories {
    maven("https://repo.kotlin.link")
    //uncomment to access development builds
    //maven("https://maven.pkg.jetbrains.space/spc/p/sci/dev")
    mavenCentral()
}

dependencies {
    implementation("space.kscience:controls-server:0.2.0")
}
```
