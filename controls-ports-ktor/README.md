# Module controls-ports-ktor

Implementation of byte ports on top os ktor-io asynchronous API

## Usage

## Artifact:

The Maven coordinates of this project are `space.kscience:controls-ports-ktor:0.2.0`.

**Gradle Kotlin DSL:**
```kotlin
repositories {
    maven("https://repo.kotlin.link")
    //uncomment to access development builds
    //maven("https://maven.pkg.jetbrains.space/spc/p/sci/dev")
    mavenCentral()
}

dependencies {
    implementation("space.kscience:controls-ports-ktor:0.2.0")
}
```
