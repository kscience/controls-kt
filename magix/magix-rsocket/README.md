# Module magix-rsocket

Magix endpoint (client) based on RSocket

## Usage

## Artifact:

The Maven coordinates of this project are `space.kscience:magix-rsocket:0.2.0`.

**Gradle Kotlin DSL:**
```kotlin
repositories {
    maven("https://repo.kotlin.link")
    //uncomment to access development builds
    //maven("https://maven.pkg.jetbrains.space/spc/p/sci/dev")
    mavenCentral()
}

dependencies {
    implementation("space.kscience:magix-rsocket:0.2.0")
}
```
