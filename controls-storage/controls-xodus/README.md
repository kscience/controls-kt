# Module controls-xodus

An implementation of controls-storage on top of JetBrains Xodus.

## Usage

## Artifact:

The Maven coordinates of this project are `space.kscience:controls-xodus:0.2.0`.

**Gradle Kotlin DSL:**
```kotlin
repositories {
    maven("https://repo.kotlin.link")
    //uncomment to access development builds
    //maven("https://maven.pkg.jetbrains.space/spc/p/sci/dev")
    mavenCentral()
}

dependencies {
    implementation("space.kscience:controls-xodus:0.2.0")
}
```
